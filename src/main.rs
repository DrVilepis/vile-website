use regex::Regex;
use rocket::fs::FileServer;
use rocket::serde::Serialize;
use rocket_dyn_templates::Template;
use chrono::naive::NaiveDate;
use serde_json::json;

#[macro_use]
extern crate rocket;

#[derive(Debug, Serialize)]
struct Context {
    blogitems: Vec<BlogEntry>,
}

#[derive(Debug, Serialize)]
struct BlogEntry {
    path: String,
    title: String,
    date: NaiveDate,
}

#[derive(Debug, Serialize)]
struct Testing {
    time: String,
}

#[get("/blog")]
fn blogroot() -> Template {
    // This probably _can_ be done better, but regex was easy
    let titleregex = Regex::new(r#"<h2>(.*?)</h2>[\s\S].*?<h class="date">(.*?)</h>"#).unwrap();
    let mut blogitems = std::fs::read_dir("templates/blog")
        .unwrap()
        .map(|path| {
            let path = path.unwrap().path();
            let mut blogpath = String::from("/blog/");
            blogpath.push_str(
                path.display()
                    .to_string()
                    .split('/')
                    .last()
                    .unwrap()
                    .split('.')
                    .next()
                    .unwrap(),
            );
            let file = &std::fs::read_to_string(path).unwrap();
            let matches = titleregex.captures(file).unwrap();
            BlogEntry {
                path: blogpath,
                title: String::from(&matches[1]),
                date: chrono::naive::NaiveDate::parse_from_str(&matches[2],"%Y-%m-%d").unwrap(),
            }
        })
        .collect::<Vec<_>>();
    blogitems.sort_by(|a,b| a.date.cmp(&b.date));
    let context = Context {
        blogitems,
    };
    Template::render("blog", &context)
}

#[get("/<blog>")]
fn blog(blog: String) -> Template {
    let mut blogpath = String::from("blog/");
    blogpath.push_str(&blog);
    Template::render(blogpath, json!({}))
}

#[get("/")]
async fn index() -> Template {
    Template::render("index", json!({}))
}

#[rocket::launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/static", FileServer::from("static"))
        .mount("/", routes![index])
        .mount("/", routes![blogroot])
        .mount("/blog", routes![blog])
        .attach(Template::fairing())
}
